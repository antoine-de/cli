from pathlib import Path
from dataclasses import dataclass, field, asdict
from typing import List
import requests
from rich import print


def upload(path: Path, geovisio: str, dry_run: bool):
    sequence = _read_sequence(path)

    _check_sequence(sequence)

    _publish(sequence, geovisio, dry_run)


@dataclass
class Picture:
    path: str


@dataclass
class Sequence:
    title: str
    pictures: List[Picture] = field(default_factory=lambda: [])


def _publish(sequence: Sequence, geovisio: str, dry_run: bool):
    print(f"publishing {sequence.title}")

    if dry_run:
        import yaml

        print(yaml.dump(asdict(sequence)))
        return

    seq = requests.post(f"{geovisio}/api/collections", {"title": sequence.title})
    seq.raise_for_status()

    seq_location = seq.headers.get("Location")
    print(f"sequence {seq_location} created")

    for i, p in enumerate(sequence.pictures, start=1):
        print(f"posting picture {p.path}")
        picture_response = requests.post(
            f"{seq_location}/items",
            files={"picture": open(p.path, "rb")},
            data={"position": i},
        )
        picture_response.raise_for_status()

    status = requests.get(f"{seq_location}/geovisio_status")
    status.raise_for_status()
    print(f"sequence status:\n{status.json()}")


def _check_sequence(sequence: Sequence):
    if not sequence.title:
        raise Exception(f"Sequence has no valid title, cannot publish")
    if not sequence.pictures:
        raise Exception(f"No pictures to upload for sequence {sequence.title}")


def _read_sequence(path: Path) -> Sequence:
    if not path.is_dir():
        raise Exception(f"{path} is not a directory, cannot read pictures")

    s = Sequence(title=path.name)

    for f in path.iterdir():
        if not f.is_file():
            continue
        if f.suffix.lower() not in [".jpg", "jpeg"]:
            continue
        s.pictures.append(Picture(path=str(f)))

    return s
