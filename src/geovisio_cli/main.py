import typer
from pathlib import Path
from geovisio_cli import upload_sequence


app = typer.Typer()


@app.callback()
def callback():
    pass


@app.command()
def upload(path: Path = typer.Option(...), geovisio: str = typer.Option(...), dry_run: bool = typer.Option(default=False)):
    upload_sequence.upload(path, geovisio, dry_run)
